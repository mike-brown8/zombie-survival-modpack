# zombie-survival-modpack

僵尸生存整合包官方BUG、建议反馈与其他相关内容仓库

注：本整合包仅支持在Windows平台运行。如需在其他平台运行，请先在Windows平台安装完更新，然后再移动到其他平台，并删除"InGameIME"模组。

问题反馈和建议请见[此Issue](https://gitea.com/mike-brown8/zombie-survival-modpack/issues/1)